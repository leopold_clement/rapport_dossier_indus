  CREATE TABLE software_present (
    id bigint(20) AUTO_INCREMENT PRIMARY KEY,
    hardware_id bigint(20) NOT NULL,
    software_possible_id bigint(20) NOT NULL,
    version varchar(255) NOT NULL
  );

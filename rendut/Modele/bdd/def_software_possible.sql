CREATE TABLE software_possible (
  id bigint(20) AUTO_INCREMENT PRIMARY KEY,
  nom varchar(31) NOT NULL,
  commande varchar(255) NOT NULL,
  regex varchar(255) NOT NULL
);
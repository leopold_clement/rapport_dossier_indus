UML_sources=$(wildcard UML/*.puml)
UML_EPS=$(UML_sources:.puml=.eps)
PLANTUML_URL = https://github.com/plantuml/plantuml/releases/download/v1.2022.5/plantuml-1.2022.5.jar

TEX_appendix=$(wildcard Appendix/*.tex)
bdd_appendix=$(wildcard bdd/*)
Images_base=$(wildcard Pictures/*)

all : rapport.pdf soutenance.pdf

rapport.pdf : rapport.tex $(Images_base) $(UML_EPS) $(TEX_appendix) $(bdd_appendix) LegrandOrangeBook.cls
	lualatex -shell-escape rapport.tex
	makeindex rapport.idx -s indexstyle.ist
	biber rapport
	lualatex -shell-escape rapport.tex
	lualatex -shell-escape rapport.tex

clean : clean-rapport clean_uml clean-soutenance_intermediaire clean-soutenance
	rm -f rapport.pdf
	rm -f soutenance_intermediaire.pdf
	rm -f plantuml.jar
	rm -f Cameo/*to.pdf

clean-rapport :
	rm -f rapport.aux
	rm -f rapport.bbl
	rm -f rapport.bib
	rm -f rapport.blg
	rm -f rapport.out
	rm -f rapport.log
	rm -f rapport.toc
	rm -f rapport.lof
	rm -f rapport.lot
	rm -f rapport.lol
	rm -f rapport.ist
	rm -f rapport.glg
	rm -f rapport.gls
	rm -f rapport.glo
	rm -f texput.log
	rm -f rapport-blx.bib
	rm -f rapport.bcf
	rm -f rapport.idx
	rm -f rapport.ptc
	rm -f -r _minted-rapport
	rm -f rapport.ilg
	rm -f rapport.ind
	rm -f rapport.run.xml
	rm -f -r TP/__pycache__

soutenance_intermediaire.pdf : soutenance_intermediaire.tex $(Images_base) $(UML_EPS)
	lualatex -shell-escape soutenance_intermediaire.tex
	lualatex -shell-escape soutenance_intermediaire.tex
	lualatex -shell-escape soutenance_intermediaire.tex

soutenance.pdf : soutenance.tex $(Images_base) $(UML_EPS) $(TEX_appendix) $(bdd_appendix)
	lualatex -shell-escape soutenance.tex
	lualatex -shell-escape soutenance.tex
	lualatex -shell-escape soutenance.tex

clean-soutenance_intermediaire :
	rm -f soutenance_intermediaire.aux
	rm -f soutenance_intermediaire.bbl
	rm -f soutenance_intermediaire.bib
	rm -f soutenance_intermediaire.blg
	rm -f soutenance_intermediaire.out
	rm -f soutenance_intermediaire.log
	rm -f soutenance_intermediaire.toc
	rm -f soutenance_intermediaire.lof
	rm -f soutenance_intermediaire.lot
	rm -f soutenance_intermediaire.lol
	rm -f soutenance_intermediaire.ist
	rm -f soutenance_intermediaire.glg
	rm -f soutenance_intermediaire.gls
	rm -f soutenance_intermediaire.glo
	rm -f soutenance_intermediaire.nav
	rm -f soutenance_intermediaire.snm
	rm -f soutenance_intermediaire.vrb
	rm -f texput.log
	rm -f -r _minted-soutenance_intermediaire

clean-soutenance :
	rm -f soutenance.aux
	rm -f soutenance.bbl
	rm -f soutenance.bib
	rm -f soutenance.blg
	rm -f soutenance.out
	rm -f soutenance.log
	rm -f soutenance.toc
	rm -f soutenance.lof
	rm -f soutenance.lot
	rm -f soutenance.lol
	rm -f soutenance.ist
	rm -f soutenance.glg
	rm -f soutenance.gls
	rm -f soutenance.glo
	rm -f soutenance.nav
	rm -f soutenance.vrb
	rm -f soutenance.snm
	rm -f texput.log
	rm -f -r _minted-soutenance

UML/%.eps : UML/%.puml plantuml.jar
	java -jar plantuml.jar -teps $<

plantuml.jar :
	wget -O plantuml.jar $(PLANTUML_URL)

clean_uml :
	rm -f UML/*.eps
	rm -f UML/*.pdf
import TP.MachineAEncoder as MachineAEncoder
import Correction as Formating
import Parsing_prof as Parsing

machine_initial = MachineAEncoder.M1
print(machine_initial)

xml = Formating.toXML_total(machine_initial)
print(xml)

machine_reconstruite = Parsing.lecteurXML(xml)
print(machine_reconstruite)
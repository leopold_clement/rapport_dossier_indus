import Modelisation
import TP.MachineAEncoder as MachineAEncoder


def toXML_1(machine: Modelisation.Machine) -> str:
    return "<Machine><nom>{}</nom><ip>{}</ip><nbRAM>{}</nbRAM></Machine>".format(machine.nom, machine.ip, machine.nbRAM)


def toXML_2(machine: Modelisation.Machine) -> str:
    return f"""<Machine nom="{machine.nom}" ip="{machine.ip}" nbRAM="{machine.nbRAM}"/>"""


def toXML_proc(proc: Modelisation.Processeur) -> str:
    return f"""<Processeur modele="{proc.modele}" frequence="{proc.frequence}"/>"""


def toXML_RAM(ram: Modelisation.BarretteRAM, id :int=0) -> str:
    return f"""<RAM id="{id}" modele="{ram.modele}" taille="{ram.taille}"/>"""


def toXML_disk(disk: Modelisation.DisqueDur) -> str:
    return f"""<Disk modele="{disk.modele}" taille="{disk.taille}" pourcentageUtilise="{disk.pourcentageUtilise}"/>"""

def toXML_total(machine: Modelisation.Machine) -> str:
    proc = toXML_proc(machine.processeur)
    rams = ""
    for id in range(machine.nbRAM):
        ram = machine.getRAM(id)
        if isinstance(ram, Modelisation.BarretteRAM):
            rams += "\t" + toXML_RAM(ram, id) + "\n"
    disks = ""
    for disk in machine.stockage:
        disks += "\t" + toXML_disk(disk) + "\n"

    return f"""
<Machine nom="{machine.nom}" ip="{machine.ip}" nbRAM="{machine.nbRAM}">
\t{proc}\n
{rams}
{disks}
</Machine>"""

ensembleTest = [MachineAEncoder.M1, MachineAEncoder.M2]
funxXMLMachine = [toXML_1, toXML_2, toXML_total]
if __name__ == "__main__":
    for fonction in funxXMLMachine:
        for machine in ensembleTest:
            print(fonction(machine))
    
XML1 = toXML_total(MachineAEncoder.M1)
XML2 = toXML_total(MachineAEncoder.M2)

from defusedxml.ElementTree import fromstring
from sympy import Mod
import Modelisation


def lecteurXML(xml: str) -> Modelisation.Machine:
    balise_machine = fromstring(xml)
    rams = [] # tuple[int, Modelisation.BarretteRAM], pour stocker l'emplacement des barrettes de RAM.
    disks = []
    for balise in balise_machine:
        if balise.tag == "Processeur":
            proc = Modelisation.Processeur(
                balise.attrib['modele'], float(balise.attrib["frequence"]))
        if balise.tag == "RAM":
            rams.append((int(balise.attrib["id"]), Modelisation.BarretteRAM(
                balise.attrib['modele'], int(balise.attrib["taille"]))))
        if balise.tag == "Disk":
            disks.append(Modelisation.DisqueDur(
                balise.attrib['modele'], int(balise.attrib["taille"]), float(balise.attrib['pourcentageUtilise'])))

    machine = Modelisation.Machine(
        balise_machine.attrib['nom'], balise_machine.attrib['ip'], proc, int(balise_machine.attrib['nbRAM']), disks)
    for id, ram in rams:
        machine.brancherRAM(id, ram)
    return machine

import Modelisation

p1 = Modelisation.Processeur("i9", 5)
p2 = Modelisation.Processeur("i3", 3)

ram1 = Modelisation.BarretteRAM("Grosse", 16)
ram2 = Modelisation.BarretteRAM("Moyenne", 8)
ram3 = Modelisation.BarretteRAM("Petite", 4)

disk1 = Modelisation.DisqueDur("SSD C", 500, 84)
disk2 = Modelisation.DisqueDur("HD D", 1000, 15)
disk3 = Modelisation.DisqueDur("SSD C", 500, 63)

M1 = Modelisation.Machine("Station de travail",
                          "234.21.2.45", p1, 2, [disk1, disk2])
M1.brancherRAM(0, ram1)

M2 = Modelisation.Machine("Portable", "234.21.23.24", p2, 2, [disk3])
M2.brancherRAM(0, ram2)
M2.brancherRAM(1, ram3)

if __name__ == "__main__":
    print(M1)
    print(M2)

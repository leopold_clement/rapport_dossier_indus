from dataclasses import dataclass
import typing


@dataclass
class Processeur():
    """Classe représentant un precesseur"""

    modele: str
    frequence: float


@dataclass
class BarretteRAM():
    """Classe représentant une barrette de RAM"""

    modele: str
    taille: int


@dataclass
class DisqueDur():
    """Classe représentant un disque dur"""

    modele: str
    taille: int
    pourcentageUtilise: float


class Machine():
    """Classe représentant une machine informatique"""

    def __init__(self, nom: str, ip: str, processeur: Processeur, nbRAM: int, stockage: typing.List[DisqueDur]) -> None:
        """Constructeur de la classe Machine

        Args:
            nom (str): Nom de la Machine
            ip (str): IP de la machine
            processeur (Processeur): Processeur de la machine
            nbRAM (int): Nombre d'emplacement de RAM
            stockage (typing.List[DisqueDur]): Liste des disques durs
        """
        self._nom = nom
        self._ip = ip
        self._processeur = processeur
        self._nbRAM = nbRAM
        self._RAM = [None]*nbRAM
        self._stockage = stockage

    @property
    def nom(self) -> str:
        return self._nom

    @property
    def ip(self) -> str:
        return self._ip

    @property
    def processeur(self) -> Processeur:
        return self._processeur

    @property
    def stockage(self) -> typing.List[DisqueDur]:
        return self._stockage

    @property
    def nbRAM(self) -> int:
        return self._nbRAM

    @nom.setter
    def nom(self, newNom: str) -> None:
        self._nom = newNom

    @ip.setter
    def ip(self, newIP: str) -> None:
        self._ip = newIP

    @processeur.setter
    def processeur(self, newProcesseur: Processeur) -> None:
        self._processeur = newProcesseur

    def checkEmplacement(self, emplacement) -> None:
        if (emplacement < 0) or (emplacement >= self._nbRAM):
            raise ValueError("Emplacement inexistant")

    def brancherRAM(self, emplacement: int, barrette: BarretteRAM) -> None:
        """Branche une barrette à un emplacement de la Machine

        Args:
            emplacement (int): Numéro de l'emplacement
            barrette (BarretteRAM): Barrette à branchée

        Raises:
            ValueError: Si il y a déjà une barrette à cet emplacement
        """
        self.checkEmplacement(emplacement)
        if isinstance(self._RAM[emplacement], BarretteRAM):
            raise ValueError("Emplacement déjà occupé")
        self._RAM[emplacement] = barrette

    def getRAM(self, emplacement: int) -> BarretteRAM:
        """Obtient une représentation de la barrette à un emplacement de la machine

        Args:
            emplacement (int): Numéro de l'emplacement

        Returns:
            BarretteRAM: Barrette à cet emplacement
        """
        self.checkEmplacement(emplacement)
        return self._RAM[emplacement]

    def debrancherRAM(self, emplacement: int) -> BarretteRAM:
        """Retire une barrette d'un emplacement

        Args:
            emplacement (int): Numéro de l'emplacement

        Raises:
            ValueError: Si il n'y a pas de barrette à l'emplacement demandé

        Returns:
            BarretteRAM: Barrette résente à l'emplacement
        """
        self.checkEmplacement(emplacement)
        if not isinstance(self._RAM[emplacement], BarretteRAM):
            raise ValueError("Pas de barrette à cet emplacement")
        barrette = self._RAM[emplacement]
        self._RAM[emplacement] = None
        return barrette

    def __repr__(self):
        return f"Machine {self.nom} @ {self.ip}, {self.processeur}, {[self.getRAM(id) for id in range(self.nbRAM)]}, {self.stockage}"
